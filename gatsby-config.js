module.exports = {
  siteMetadata: {
    title: `Emi | Your Relationship Reminder`,
    description: `Short, daily exercises delivered through text. Take a minute out of your hectic day, and start building happy habits.`,
    author: `@gatsbyjs`,
  },
  plugins: [
    `gatsby-plugin-react-helmet`,
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `images`,
        path: `${__dirname}/src/images`,
      },
    },
    `gatsby-transformer-sharp`,
    `gatsby-plugin-sharp`,
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: `Emi | Your Relationship Reminder`,
        short_name: `emicouple`,
        start_url: `/`,
        background_color: `#B19DD1`,
        theme_color: `#B19DD1`,
        display: `minimal-ui`,
        // icon: `src/images/gatsby-icon.png`, // This path is relative to the root of the site.
      },
    },

    // this (optional) plugin enables Progressive Web App + Offline functionality
    // To learn more, visit: https://gatsby.dev/offline
    // `gatsby-plugin-offline`,
  ],
}
