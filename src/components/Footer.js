import React, { useState, useEffect } from "react"
import send from "../images/send.svg"

import facebook from "../images/facebook.svg"
import instagram from "../images/instagram.svg"
import TextInput from "./textInput"

export default function Footer() {
  const [inputValue, setInputValue] = useState("")
  const [submitted, setSubmitted] = useState(false)
  //cookie popup agreement
  const [agreement, setAgreement] = useState(null)

  const setLocalStorage = () => {
    setAgreement("agreed")
    localStorage.setItem("consent", "agreed")
    console.log(agreement)
  }

  useEffect(() => {
    var consent = localStorage.getItem("consent")
    if (consent !== "agreed") {
      setAgreement("notAgreed")
    }
  })

  const handleChange = e => {
    e.preventDefault()
    setInputValue(e.target.value)
  }
  const handleSubmit = e => {
    e.preventDefault(e)
    setTimeout(() => setSubmitted(true), 2000)
  }

  return (
    <div>
      <footer>
        <div className="container">
          <p className="sign-up-for-our-news">Sign up for our newsletter</p>
          <div className="footer-box">
            <div className="newsletter">
              <div
                className="email-textbox-section"
                style={!submitted ? { display: "block" } : { display: "none" }}
              >
                <form
                  onSubmit={e => {
                    handleSubmit(e)
                  }}
                >
                  <TextInput onChange={handleChange} inputValue={inputValue} />
                  <button className="send-btn">
                    <img src={send} />
                  </button>
                </form>
              </div>
              <div
                className="thank-you-for-reques "
                style={submitted ? { display: "block" } : { display: "none" }}
              >
                You're all set!
              </div>
            </div>
            <div className="socai-media">
              <a href="https://www.facebook.com/emicouple" target="_blank">
                <img src={facebook} alt="facebook" />
              </a>
              <a href="https://www.instagram.com/emicouple/" target="_blank">
                <img src={instagram} alt="instagram" />
              </a>
              <div className="emi-inc-all">
                © 2019 Emi Inc. All Rights Reserved.{" "}
              </div>
            </div>
            <div className="ters-cond-box">
              <a href="">Terms &amp; Conditions</a>
              <a href="">Privacy Policy</a>
            </div>
          </div>
        </div>
      </footer>
      <div
        className="cookies-section"
        style={
          agreement === "notAgreed" ? { display: "block" } : { display: "none" }
        }
      >
        <span>
          🍪By browsing this website, you consent to our Privacy Policy —
        </span>
        <span className="iAgree" onClick={setLocalStorage}>
          I agree
        </span>
      </div>
    </div>
  )
}
