import React, { useState } from "react"
import TextInput from "./textInput"

export default function CtaFooter() {
  const [inputValue, setInputValue] = useState("")
  const [submitted, setSubmitted] = useState(false)

  const handleChange = e => {
    e.preventDefault()
    setInputValue(e.target.value)
  }
  const handleSubmit = e => {
    e.preventDefault(e)
    setTimeout(() => setSubmitted(true), 2000)
  }

  return (
    <div className="downloadapp-section-box">
      <p>
        Life gets busy, so we’re here to make this simple for you. Build healthy
        relationship habits with Emi.
      </p>
      <div
        className="homepage-email-textbox-section"
        style={!submitted ? { display: "block" } : { display: "none" }}
      >
        <form
          onSubmit={e => {
            handleSubmit(e)
          }}
        >
          <TextInput onChange={handleChange} inputValue={inputValue} />
          <button className="send-btn">Request access</button>
        </form>
      </div>
      <div
        className="thank-you-for-reques"
        style={submitted ? { display: "block" } : { display: "none" }}
      >
        Thank you for requesting access. We will follow up with an email with a
        code and a link to our app!
      </div>
    </div>
  )
}
