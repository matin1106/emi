import React, { useState } from "react"
import TextInput from "./textInput"

export default function CtaHeader() {
  const [inputValue, setInputValue] = useState("")
  const [submitted, setSubmitted] = useState(false)

  const handleChange = e => {
    e.preventDefault()
    setInputValue(e.target.value)
  }
  const handleSubmit = e => {
    e.preventDefault(e)
    setTimeout(() => setSubmitted(true), 2000)
  }

  return (
    <div className="your-daily-relations left-side">
      <p>Your daily relationship reminder</p>
      <div
        className="homepage-email-textbox-section"
        style={!submitted ? { display: "block" } : { display: "none" }}
      >
        <form
          onSubmit={e => {
            handleSubmit(e)
          }}
        >
          {/* <input
            type="email"
            placeholder="Enter your email"
            className="enter-your-email"
            style={isActive ? { borderBottom: "1px solid #FB8F70" } : null}
            onChange={e => handleChange(e)}
          /> */}
          <TextInput onChange={handleChange} inputValue={inputValue} />
          <button className="send-btn" type="submit">
            Request access
          </button>
        </form>
      </div>
      <div
        className="thank-you-for-reques"
        style={submitted ? { display: "block" } : { display: "none" }}
      >
        Thank you for requesting access. We will follow up with an email with a
        code and a link to our app!
      </div>
    </div>
  )
}
