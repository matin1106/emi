import { Link } from "gatsby"
import PropTypes from "prop-types"
import React, { useState, useEffect } from "react"
import SEO from "../components/seo"
import logo from "../images/logo.svg"
import menu from "../images/mobile_menu.svg"
import appstore from "../images/appstore.svg"
import closeIcon from "../images/close.svg"
import facebook from "../images/facebook.png"
import instagram from "../images/instagram.png"

// todo:- optimize onscroll event with debounce funct

// menu sidebar styles
const open = {
  width: "100%",
}
const close = {
  width: "0",
}

//Sticky Menu Style
const visible = {
  top: "0px",
}

const hidden = {
  top: "-100px",
}

let headerRef = React.createRef()

// header component
const Header = props => {
  // Below state it to preserve mobile menu
  const [isOpen, setIsOpen] = useState(false)
  const [isSticky, setIsSticky] = useState(false)
  const [headerOffSet, setHeaderOffset] = useState(null)

  const toggleMenu = () => {
    setIsOpen(!isOpen)
  }
  const handleScroll = () => {
    window.pageYOffset > 30 ? setIsSticky(true) : setIsSticky(false)
  }

  useEffect(() => {
    setHeaderOffset(headerRef.current.offsetTop)
    window.addEventListener("scroll", handleScroll)
  })

  return (
    <>
      <SEO />
      <header className="" ref={headerRef}>
        <div className="container">
          <div className="header-section">
            <div className="logo">
              <Link to="/" activeClassName="active">
                <img src={logo} alt="EMI logo" title="EMI Logo" />
              </Link>
            </div>
            <nav className="web-menu">
              <ul>
                <li>
                  <Link to="/about" activeClassName="active">
                    About Us
                  </Link>
                </li>
                <li>
                  <a href="https://medium.com/emi-couple" target="_blank">
                    Blog
                  </a>
                </li>
                <li>
                  <Link to="/faq" activeClassName="active">
                    FAQ
                  </Link>
                </li>
              </ul>
            </nav>
            <div className="download-app">
              <a href="https://apps.apple.com/us/app/emi-relationship-reminder/id1478777207?ls=1">
                <img src={appstore} />
              </a>
            </div>
            <div className="mobile-menu-icon" onClick={() => toggleMenu()}>
              <img src={menu} />
            </div>

            <div
              id="myHeader"
              className="sticky-header"
              style={isSticky ? visible : hidden}
            >
              <div className="container">
                <div className="header-section">
                  <div className="logo">
                    <Link to="/" activeClassName="active">
                      <img src={logo} alt="EMI logo" title="EMI Logo" />
                    </Link>
                  </div>
                  <nav className="web-menu">
                    <ul>
                      <li>
                        <Link to="/about" activeClassName="active">
                          About Us
                        </Link>
                      </li>
                      <li>
                        <a href="https://medium.com/emi-couple" target="_blank">
                          Blog
                        </a>
                      </li>
                      <li>
                        <Link to="/faq" activeClassName="active">
                          FAQ
                        </Link>
                      </li>
                    </ul>
                  </nav>
                  <div className="download-app">
                    <a href="https://apps.apple.com/us/app/emi-relationship-reminder/id1478777207?ls=1">
                      <img src={appstore} />
                    </a>
                  </div>
                  <div
                    className="mobile-menu-icon"
                    onClick={() => toggleMenu()}
                  >
                    <img src={menu} />
                  </div>
                </div>
              </div>
            </div>
            <div
              id="mySidepanel"
              className="mobile-panel"
              style={isOpen ? open : close}
            >
              <div className="mobile-panel-box">
                <div className="mobile-panel-heading">
                  <a href="index.html">
                    <img src={logo} />
                  </a>
                  <a
                    href="javascript:void(0)"
                    className="menu-close"
                    onClick={() => toggleMenu()}
                  >
                    <img src={closeIcon} />
                  </a>
                </div>
                <div className="menu-navigation">
                  <nav>
                    <ul>
                      <li className="active">
                        <Link to="/about" activeClassName="active">
                          About Us
                        </Link>
                      </li>
                      <li>
                        <a href="https://medium.com/emi-couple" target="_blank">
                          Blog
                        </a>
                      </li>
                      <li>
                        <Link to="/faq" activeClassName="active">
                          FAQ
                        </Link>
                      </li>
                    </ul>
                  </nav>
                  <div className="socai-media">
                    <a
                      href="https://www.facebook.com/emicouple"
                      target="_blank"
                    >
                      <img src={facebook} alt="facebook" />
                    </a>
                    <a
                      href="https://www.instagram.com/emicouple/"
                      target="_blank"
                    >
                      <img src={instagram} alt="instagram" />
                    </a>
                  </div>
                  <div className="download-app">
                    <a href="https://apps.apple.com/us/app/emi-relationship-reminder/id1478777207?ls=1">
                      <img src={appstore} />
                    </a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </header>
    </>
  )
}

export default Header
