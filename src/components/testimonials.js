import React from "react"
import Swiper from "react-id-swiper"
import "react-id-swiper/lib/styles/css/swiper.css"

export default function Testimonials() {
  const testimonials = [
    {
      name: "Will",
      relationship: "married 8 years",
      comment:
        "Emi made me be more mindful of how my partner was helping me and how I was helping my partner each day.",
    },
    {
      name: "Rainie",
      relationship: "married 7 years",
      comment: "Emi reminds us to do something simple and small but important.",
    },
    {
      name: "Alexandra",
      relationship: " married 16 years",
      comment:
        "Emi made me break my habits by asking me to do things.  I felt I was thinking more about our relationship and how to foster it in a positive way.  ",
    },
    {
      name: "Phil",
      relationship: "married 8 years",
      comment: "Emi creates a little time for us in the chaos of our lives.  ",
    },
    {
      name: "Rebecca",
      relationship: null,
      comment:
        "My spouse and I have been using Emi, and I really think it's improved our communication and relationship.",
    },
    {
      name: "Cathryn",
      relationship: "married 10-14 yrs",
      comment:
        "Emi has been fun for us. Emi has definitely nudged us closer in just a few weeks",
    },
    {
      name: "Tracy",
      relationship: "married 7 years",
      comment:
        "Emi reminds me to be grateful but also introduces a little sizzle in our relationship.  ",
    },
    {
      name: "Amy",
      relationship: null,
      comment:
        "Emi has been a HUGE help in my marriage, thank you for all the work you do!",
    },
  ]
  const params = {
    slidesPerView: "auto",
    autoplay: {
      delay: 3000,
    },
    pagination: {
      el: ".swiper-pagination",
      type: "bullets",
      dynamicBullets: true,
      clickable: true,
    },
    navigation: {
      nextEl: ".swiper-button-next",
      prevEl: ".swiper-button-prev",
    },
    spaceBetween: 30,
  }
  return (
    <section className="testimonial">
      <Swiper {...params}>
        {testimonials.map(slide => {
          return (
            <div className="container" style={{ maxWidth: "none" }}>
              <div className="testimonial-box">
                <div className="carousel-inner">
                  <ul>
                    <li>
                      <div className="testimonial-content">{slide.comment}</div>
                      <div className="testimonial-descp">
                        <p>{slide.name ? slide.name : null}</p>
                        <article>{slide.relationship}</article>
                      </div>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          )
        })}
      </Swiper>
    </section>
  )
}
