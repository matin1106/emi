import React, { useState } from "react"

export default function TextInput(props) {
  const { inputValue, onChange, ...InputProps } = props

  return (
    <input
      type="email"
      placeholder="Enter your email"
      className="enter-your-email"
      style={inputValue ? { borderBottom: "1px solid #FB8F70" } : null}
      onChange={onChange}
      {...InputProps}
    />
  )
}
