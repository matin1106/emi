import React from "react"
import Header from "../components/header"
import "../styles/global.css"

const NotFoundPage = () => {
  return (
    <div className="wrapper">
      <div className="erroe404-page">
        <Header />
        <section>
          <div className="container">
            <div className="about-us-text">
              <h1 className="strong-relationships">
                Hmm, that page doesn't exist
              </h1>
              <p className="custom-btn">Let's go home</p>
            </div>
          </div>
        </section>
      </div>
    </div>
  )
}

export default NotFoundPage
