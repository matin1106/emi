import React from "react"
import Header from "../components/header"
import Footer from "../components/Footer"
import heartImage from "../images/about-us-heart.svg"
import heartImage2 from "../images/about-us-heart-2.svg"
import aya from "../images/Team-Aya-photo.png"
import hiroki from "../images/Team-Hiroki-photo.png"
import dominique from "../images/Team-Dominique-photo.png"

export default function about() {
  return (
    <>
      <div className="wrapper">
        <div className="aboutus-section-1">
          <Header />
          <section>
            <div className="container">
              <div className="about-us-text">
                <h1 className="strong-relationships">
                  Strong Relationships = Health and Happiness
                </h1>
                <p className="live-happier-and-hea">
                  Live happier and healthier with Emi
                </p>
              </div>
            </div>
          </section>
        </div>
        <section className="aboutus-section-2 clearfix">
          <img src={heartImage} className="heart-img" />
          <div className="container">
            <h2 className="emi-means-smile-in">
              Emi means “smile” in Japanese
            </h2>
            <div className="details-text">
              <span>
                Emi was born out of life experience. We all want our
                relationship with our partner to be great, but maintaining a
                happy and healthy relationship sounds like a lot of work. We
                want to make this simple and easy for you, by bringing simple
                mindful practices at your fingertips.
              </span>
              <span>
                We are constantly fighting unrealistic idealistic conceptions of
                marriage and romantic relationships. At Emi, we want to make
                relationship fitness more mainstream and show every couple that
                meaningful relationships are diverse as the people that are a
                part of them. So come join our community, and build happy
                healthy relationship habits with us.
              </span>
            </div>
          </div>
          <img src={heartImage2} className="heart-img-2" />
        </section>
        <section className="aboutus-section-3">
          <div className="container">
            <div className="team-box">
              <h2 className="subheading">Meet our team</h2>
              <ul className="team-section">
                <li>
                  <img src={aya} />
                  <div className="descp">
                    <p>Aya Takeuchi</p>
                    <article>Co-Founder</article>
                  </div>
                </li>
                <li>
                  <img src={hiroki} />
                  <div className="descp">
                    <p>Hiroki Hori</p>
                    <article>Co-Founder</article>
                  </div>
                </li>
                <li>
                  <img src={dominique} />
                  <div className="descp">
                    <p>Dominique Samuels</p>
                    <article>PsyD, Adviser</article>
                  </div>
                </li>
              </ul>
            </div>
          </div>
        </section>
        <section className="aboutus-section-4">
          <div className="container">
            <div className="email-box">
              <p>
                We’re always looking for great people
                <br /> to work with! Email us at
              </p>
              <a href="mailto:hello@emicouple.com">hello@emicouple.com</a>
            </div>
          </div>
        </section>
        <Footer />
      </div>
    </>
  )
}
