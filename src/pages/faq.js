import React, { useState } from "react"
import Header from "../components/header"
import Footer from "../components/Footer"

//below element for last FAQ
const RenderHTML = () => {
  const html = '<div><a href="#">This is example</a></div>'
  return (
    <div>
      Please email us at
      <a
        style={{ margin: "0 5px", color: "#F8C5B0" }}
        href="mailto:hello@emicouple.com?Subject=business%20inquiries."
        target="_blank"
      >
        hello@emicouple.com
      </a>
      with any partnership or business inquiries.
    </div>
  )
}

const qna = [
  {
    question: "What is Emi all about?",
    answer:
      "Emi is a relationship wellness app that sends you a daily reminder to connect with your partner.  Emi will send you short, actionable reminders of things that you can do - sometimes in app or in text, and sometimes in real life - to be a better partner.",
    id: "1",
  },
  {
    question: 'What is an "exercise"?',
    answer:
      'Emi exercises are short, simple activities that you can do every day.  An example of an Emi exercise might be "take a minute to send your partner a quick text" or "don\'t forget to ask your partner how her day was".  All exercises take less than a minute to do.  You do not need your partner physically present there to do these.  ',
    id: "2",
  },
  {
    question: "How do I change the exercises that I'm receiving?",
    answer:
      "You can change, add or delete your exercises at any point through the Discover tab in your app.  You can also select multiple exercises that will go into your queue (no limits!) if you want to try out different types.  ",
    id: "3",
  },
  {
    question: "When and how often do the texts come?",
    answer:
      "You will receive a daily text reminder with your exercise of the day.  Text exercises are defaulted to a certain time when you sign up, but you can change the timing and any other notification settings at any point.  ",
    id: "4",
  },
  {
    question: "How do I change the timing of the texts? ",
    answer:
      'You can change the timing of your reminders at any point to whatever fits your schedule.  Just go to the Home tab in the app where you will see "My Exercises" - click the clock icon in each exercise to change timing.  ',
    id: "5",
  },
  {
    question: "Is Emi free?",
    answer: "Yes, Emi is currently free for all users!",
    id: "6",
  },
  {
    question: "Do I need to get my partner to sign up for Emi?",
    answer:
      "We've found that couples that join Emi together do have a higher satisfaction and see better results.  The current version of the Emi app requires you to put your partner's information in, but it's up to the partner to download the app.  Your partner will start receiving Emi exercises via text when you sign up.",
    id: "7",
  },
  {
    question: "How do I add my partner onto Emi?",
    answer:
      "Currently the Emi app requires you to sign up with your partners information.",
    id: "8",
  },
  {
    question:
      "I want my partner to do these exercises too!  How do we do that?",
    answer:
      "If you want your partner to do these exercises, you can ask them to download the Emi app.  Once they're signed up, you will start seeing their response in-app, and your partner will see yours.  ",
    id: "9",
  },
  {
    question:
      "My partner is reluctant.  How can I convince my partner to sign up for Emi?",
    answer:
      "Try something like \"Hey, I want us to try Emi.  It's a simple and fun app that sends us reminders to take 2 minutes out of our busy day to be present for each other.\"  Or, \"I'd love for you to download Emi.  You'll receive different messages from me whenever I'm being mindful about you and our relationship\"",
    id: "10",
  },
  {
    question: "My partner and I are long-distance, can we still use Emi?",
    answer:
      "Yes!  Long distance couples love Emi.  It will remind you to touch base with them and stay connected with them even if you're apart.  ",
    id: "11",
  },
  {
    question: "Will my partner see all the information I write on Emi?",
    answer:
      "No, your partner will not see everything you write/say to Emi.  We will let you know whenever your responses are shared with your partner.  ",
    id: "12",
  },
  {
    question: "Where is Emi offered?",
    answer: "Currently, the Emi app is only available in the US App store.  ",
    id: "13",
  },
  {
    question: "I don't have an iPhone, is there an Android app?",
    answer:
      "Sorry!!  Currently we only have an iOS app - but please be patient, it's coming!!",
    id: "14",
  },

  {
    question: "How can I share Emi with friends?  ",
    answer:
      'You can share Emi through your Profile tab in the app.  Click on Settings, and you will see "Share Emi".  You can also lead them to our homepage at www.emicouple.com',
    id: "15",
  },
  {
    question: "Do you share any personal information with 3rd parties?",
    answer:
      "Nope.  We never share or sell any user information without your consent. Emi will always ask your permission before sharing any information with a 3rd party partner.",
    id: "16",
  },
  {
    question: "How can we partner with Emi?",
    answer: <RenderHTML />,
    id: "17",
  },
]
const Faq = props => {
  const [activeId, setActiveId] = useState(0)

  const handleClick = id => {
    setActiveId(id)
  }

  return (
    <div className="wrapper">
      <div className="faq-section-1">
        <Header />
        <section>
          <div className="container">
            <div className="about-us-text">
              <h1 className="strong-relationships">
                Thank you for visiting our FAQ page
              </h1>
              <div className="if-you-don-t-see-you">
                <p>If you don't see your answers below, please email us at</p>
                <p>
                  <b>
                    <a
                      href="mailto:hello@emicouple.com"
                      target="_blank"
                      style={{ color: "white" }}
                    >
                      hello@emicouple.com
                    </a>
                  </b>{" "}
                  and we'll get back to you as
                </p>
                <p>soon as we can!</p>
              </div>
            </div>
          </div>
        </section>
      </div>
      <section className="faq-section-2 clearfix">
        <div className="container">
          <h2 className="emi-text-faq">Emi Text FAQ</h2>
          <div className="">
            <div className="accordionWrapper">
              {qna.map(faq => {
                return (
                  <div
                    className={
                      activeId === faq.id
                        ? "accordionItem open"
                        : "accordionItem close"
                    }
                    onClick={() => handleClick(faq.id)}
                    key={faq.id}
                  >
                    <h2 className="accordionItemHeading">{faq.question}</h2>
                    <div className="accordionItemContent">{faq.answer}</div>
                  </div>
                )
              })}
            </div>
          </div>
        </div>
      </section>

      <Footer />
    </div>
  )
}
export default Faq
