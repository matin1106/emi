import React from "react"
import Header from "../components/header"
import Testimonials from "../components/testimonials"

import SEO from "../components/seo"
import { Link } from "gatsby"
import phoneMain from "../images/phoneMain.svg"
import mobile2 from "../images/screen02.svg"
import mobile3 from "../images/phone03.svg"
import mobile4 from "../images/phone04.svg"

import Footer from "../components/Footer"
import CtaHeader from "../components/cta_header"
import CtaFooter from "../components/cta_footer"

const IndexPage = () => (
  <>
    <SEO />
    <div className="wrapper homepage-wrapper">
      <div className="homepage-section-1">
        <Header />
        <div className="banner">
          <div className="container">
            <div className="image-text-section">
              <CtaHeader />
              <div className="image-section right-side">
                <img src={phoneMain} />
              </div>
            </div>
          </div>
        </div>
      </div>
      <section className="homepage-section-2">
        <div className="container">
          <div className="image-text-section">
            <div className="image-section left-side">
              <img src={mobile2} />
            </div>
            <div className="descp-section right-side">
              <h2>
                Choose from a variety of exercises that fits your relationship
              </h2>
              <p>Exercises vetted by relationship experts</p>
            </div>
          </div>
        </div>
      </section>
      <section className="homepage-section-3">
        <div className="container">
          <div className="image-text-section">
            <div className="descp-section left-side">
              <h2>Bite-sized exercises for busy schedules</h2>
              <p>Reply over text or in app when it’s convenient for you</p>
            </div>
            <div className="image-section right-side">
              <img src={mobile3} />
            </div>
          </div>
        </div>
      </section>
      <section className="homepage-section-4">
        <div className="container">
          <div className="image-text-section">
            <div className="image-section left-side">
              <img src={mobile4} />
            </div>
            <div className="descp-section right-side">
              <h2>Turn it into a routine</h2>
              <p>Keep track of what you’ve said and your positive thoughts</p>
            </div>
          </div>
        </div>
      </section>
      <Testimonials />
      <section className="downloadapp-section">
        <div className="container">
          <CtaFooter />
        </div>
      </section>
      <Footer />
    </div>
  </>
)

export default IndexPage
